<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="./css/stylesheet_receipt.css" />
    </head>
    <body>

    <?php

    // Get user input from order form

    $name = $_POST["name"];
    $apple = $_POST["apple"];
    $orange = $_POST["orange"];
    $banana = $_POST["banana"];

    $numericRegex = "/\d+/";
    $nameRegex = "/^[^ \r\t\n\f0-9][a-zA-Z\s]*$/";

    // server-side validation

    try {

        // validate name

        if (!preg_match($nameRegex, $name) || strlen($name) < 1) { //if length of name is < 1
            throw new UnexpectedValueException("Invalid name!");
        }

        // validate number of apples

        if (!preg_match($numericRegex, $apple) || $apple < 0) { //if apple not int or apple < 0
            throw new UnexpectedValueException("Invalid number of apple!");
        }
        // validate number of oranges

        if (!preg_match($numericRegex, $orange) || $orange < 0) { //if orange not int or orange < 0
            throw new UnexpectedValueException("Invalid number of orange!");
        }
        // validate number of bananas

        if (!preg_match($numericRegex, $banana) || $banana < 0) { //if banana not int or banana < 0
            throw new UnexpectedValueException("Invalid number of banana!");
        }
        // ensure that the usr purchase at least one fruit

        if ($apple < 1 && $orange < 1 && $banana < 1) {//if user did not purchase any fruit
            throw new UnexpectedValueException("Please purchase at least 1 fruit!");
        }
        // calculate total price of fruits in the user's order

        $totalPrice = ($apple * 0.69 + $orange * 0.59 + $banana * 0.39);

        // validate mode of payment

        if (isset($_POST["payment"])) { //if the mode of payment is selected
            $payment = $_POST["payment"];
        }
        else { // if mode of payment not selected
            throw new UnexpectedValueException("Please select a mode of payment");
        }

        // Update the total quantity of fruits in txt file

        $filename = "order.txt";
        if (!file_exists($filename)) { //create the order.txt file if it does not exist
            $appleRecord = "Total number of apples: " . $apple;
            $orangeRecord = "Total number of oranges: " . $orange;
            $bananaRecord = "Total number of bananas: " . $banana;
            $record = $appleRecord . "\n" . $orangeRecord . "\n" . $bananaRecord;
            $fh = fopen($filename, 'w');
            file_put_contents($filename, $record);
            fclose($fh);
        }
        else { //update current "order.txt" file
            $file = fopen($filename, "r");
            $record = "";
            for ($i = 0; !feof($file); ++$i) { //if not end of file
                $string = fgets($file);
                preg_match($numericRegex, $string, $matches); //match line with integer
                switch ($i) {
                case 0: //find qty of apple
                    $record.= preg_replace($numericRegex, $matches[0] + $apple, $string);
                    break;

                case 1: //find qty of orange
                    $record.= preg_replace($numericRegex, $matches[0] + $orange, $string);
                    break;

                case 2: //find qty of banana
                    $record.= preg_replace($numericRegex, $matches[0] + $banana, $string);
                    break;
                }
            }

            file_put_contents($filename, $record);
            fclose($file);
        }
    ?>


        <div class="container">

            <table id="receiptStyle" width="100%">
                <div id="rows">
                    <tr id="tableHeader"></tr>
                    <tr>
                        <td>Name </td>
                        <td>: <?php echo $name; ?></td>
                            <tr class = "table-active">
                                <td>Purchase</td>
                                <td>Quantity</td>
                                <td>Cost</td>
                            </tr>
                            <tr>
                                <td>Apples: </td>
                                <td> <?php echo $apple; ?></td>
                                <td>$<?php echo $apple*0.69; ?></td>
                            </tr>

                            <tr>
                                <td>Oranges: </td>
                                <td> <?php echo $orange; ?></td>
                                <td>$<?php echo $orange*0.59; ?></td>
                            </tr>

                            <tr>
                                <td> Bananas: </td>
                                <td> <?php echo $banana; ?></td>
                                <td>$<?php echo $banana*0.39; ?></td>
                            </tr>

                            <tr id="tableBottomSection"></tr>

                            <tr>
                                <td>Total Price: </td>
                                <td>: $<?php echo $totalPrice; ?></td>
                            </tr>

                            <tr>
                                <td>Mode of Payment</td>
                                <td>: <?php echo $payment; ?></td>
                            </tr>
                            <tr id="tableFooter"></tr>
                    </tr>


                </div>

            </table>
    <?php
    }

    catch(UnexpectedValueException $e) {
    ?>
        <h3>Error</h3>
        <label><?php echo $e->getMessage(); ?></label>
    <?php
    } ?>

        </div>

    </body>
</html>